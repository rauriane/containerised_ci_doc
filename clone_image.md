# Clone image to another container registry

Sometimes you want to get the created image out of CSCS infrastructure. For this reason there is an API endpoint that allows you to clone the image to some other registry (e.g. dockerhub.com).
You need to create a YAML file with the content
```yaml
username: YOUR_REPOSTIROY_ID
password: YOUR_WEBHOOK_SECRET
from_image: $CSCS_REGISTRY_PATH/my_cool_image:1.0
to_image: docker.io/your_dockerhub_username/my_cool_image_cloned:latest
registry_credentials:
  username: your_registry_username
  password: your_registry_password
```
- `from_image`: The image that you want to copy out of CSCS infrastructure. It should begin with `$CSCS_REGISTRY_PATH`.
- `to_image`: The registry and path where you want to clone the image to
- `registry_credentials`: The credentials which allow to login to the registry specified in `to_image`. Many registries allow you to create access tokens, which should be preferred to your password.

Once you have generated the file (let's call it `copy.yaml`), you can start the clone process by issuing a POST event to `ci/image/clone`, e.g. via curl:
```sh
curl --data-binary @copy.yaml https://cicd-ext-mw.cscs.ch/ci/image/clone
```

The call might take some time, depending on image size.

# Allow anonymous access to container image

It is possible to allow anonymous access to the container image by pushing the image to a path that matches the regexp `$CSCS_REGISTRY_PATH/.*/public/.*`.

**WARNING**: This allows within CSCS network pulling of the container without any credentials, i.e. any CSCS user could potentially pull the container image.
