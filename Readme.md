[[_TOC_]]

## Introduction
Containerised CI/CD allows you to build containers and run them at scale on CSCS systems.
The basic idea is that you provide a Dockerfile with build instructions and run the newly created container.  Most of the boiler plate work is being taken care by the CI implementation such that you can concentrate on providing build instructions and tests.  The important information is provided to you from the CI side for the configuration of your repository.

We support any git provider that supports webhooks. This includes github, gitlab and bitbucket.
A typical pipeline consists of at least one build job and one test job. The build job makes sure that a new container with your most recent code changes is built.
The test step will then use this container and run the container as an MPI job, i.e. it can run your tests on multiple nodes with GPU support.

Building your software inside a container requires a `Dockerfile` and a name for the container in the registry.  Testing your software then requires the commands that must be executed to run the tests. No explicit container spawning is required (and also not possible). Your test jobs need to specify the number of nodes and tasks required for the test and the test commands.

Here is an example of a full [helloworld project](https://github.com/finkandreas/containerised_ci_helloworld).

## Tutorial Hello World
We are using the [containerised hello world repository](https://github.com/finkandreas/containerised_ci_helloworld).
This is a sample Hello World CMake project. The application only echos `Hello from $HOSTNAME`, but the idea how to run a program on multiple nodes should be clear from this idea.
The pipeline instructions are inside the file `ci/pipeline.yml`.
Let us walk through the pipeline bit by bit.
```yaml
include:
  - remote: 'https://gitlab.com/cscs-ci/recipes/-/raw/master/templates/v2/.ci-ext.yml'
```
This block includes a yml file which contains definitions with default values to build and run containers. Have a look inside this file to see available building blocks.

```yaml
stages:
  - build
  - test
```
Here we define two different stages, named `build` and `test`. The names can be chosen freely.
```yaml
variables:
  PERSIST_IMAGE_NAME: $CSCS_REGISTRY_PATH/helloworld:$CI_COMMIT_SHORT_SHA
```
This block defines variables that will apply to all jobs.
```yaml
build_job:
  stage: build
  extends: .container-builder
  variables:
    DOCKERFILE: ci/docker/Dockerfile.build
```
This adds a job named `build_job` to the stage `build`. This runner expects a Dockerfile as input file, which is specified in the variable `DOCKERFILE`. The resulting container name is specified with the variable `PERSIST_IMAGE_NAME`, which has been defined already above, therefore it does not need to be explicitly mentioned in the `variables` block again.
There is further documention of this runner at [gitlab-runner-k8s-container-builder](https://gitlab.com/cscs-ci/ci-testing/webhook-ci/gitlab-runner-k8s-container-builder)
```yaml
test_job:
  stage: test
  extends: .container-runner-daint-gpu
  image: $PERSIST_IMAGE_NAME
  script:
    - /opt/helloworld/bin/hello
  variables:
    SLURM_JOB_NUM_NODES: 2
    SLURM_PARTITION: normal
    SLURM_NTASKS: 2
```
This block defines a test job. The job will be executed by the `daint-container` runner. This runner will pull the image on daint and run the commands as specified in the `script` tag. In this example we are running on 2 nodes with 1 task on each node, i.e. 2 tasks total. All [slurm environment variables](https://slurm.schedmd.com/srun.html#SECTION_INPUT-ENVIRONMENT-VARIABLES) are supported. The commands will be running inside the container specified by the `image` tag. The runner is [hosted here](https://gitlab.com/cscs-ci/gitlab-runner-slurm-sarus) and supports several other variables.

## Containerised CI at CSCS

### Enable CI for your project
While the procedure to enable CSCS CI for your repository consists of only a few subsequently mentioned steps, many of them require features in GitHub, GitLab or Bitbucket.  There are pointers and within the individual steps which additional steps are needed, and some of those documents are non-trivial, especially if you do not have considerably background in the repository features.  Plan sufficient time for the set-up and contact a GitHub/GitLab/Bitbucket professional, if needed.

1. **Register your project**: The first step to use containerised CI/CD is to register your Git repository with CSCS. Please contact Andreas Fink (andreas.fink@cscs.ch) for this step. Once your project has been registered at CSCS you will be provided with a project-ID and a webhook-secret.
1. **Add CI user to your CSCS project** Add the user `jenkssl` to your CSCS project group (ask your project lead if you do not have access [here](https://account.cscs.ch))
1. **Setup CI**: Head to the [CI setup page](https://cicd-ext-mw.cscs.ch/ci/setup/ui) and login with your project-ID as username and webhook-secret as password (i.e., those supplied to you by andreas.fink@cscs.ch in Step 1.)
1. **Optional: Private project**: If your Git repository is a private repository make sure to check the `Private repository` box and follow the instructions to add an SSH key to your Git repository.
1. **Add notification token**: On the setup page you will also find the field `Notification token`. Even if this field may be signaled as green (with a check mark), please add a token, such that your Git repository will be notified about the status of the build jobs. (Click on the small triangle to get further instructions of non-trivial complexity)
1. **Add webhook**: On the setup page you will find the `Setup webhook details` button. If you click on it you will see all the entries which have to be added to a new webhook in your Git repository. Follow the link indicated there to your repository, and add the webhook with the given entries..
1. **Optional: Webhook secret**: You do not need to change the field. If you are unhappy with the random webhook secret you are free to set it to another one (**not recommended to change it**)
1. **Default whitelisted users and default CI-enabled branches**: Provide the default list of whitelisted users and CI-enabled branches. The global configuration will apply to all pipelines that do not overwrite it explicitly.
1. **Pipeline default**: Your first pipeline has the name `default`. Click on `Pipeline default` to see the pipeline setup details. The name can be chosen freely but it cannot contain whitespaces (a short descriptive name). Update the entrypoint, whitelisted users and CI-enabled branches.
1. **Submit your changes**
1. **Optional: Add other pipelines**: Add other pipelines with a different entrypoint if you need more pipelines.
1. **Add entrypoint yml files to Git repository**: Commit the yml-entrypoint files to your repository. You should get notifications about the build status in your repository if everything is correct. See the [Hello World Tutorial](#tutorial-hello-world) for a simple yml-file.

#### Clarifications and pitfalls to the above-mentioned steps

The procedure above is deceptively simple:  underneath the hood extremely complicated middleware is being configured. Some of the steps assume an extensive knowledge of GitLab/GitHub functionality: the pointers to the documentation therein may require extensive reading and preparation.  If the steps are not completed rigorously, errors in the middleware will occur which are extremely difficult to debug. Here are some points to avoid the key pitfalls

- **Add webhook** Here you need to click on **Webhook setup details** (top of page) which then contains a pointer to the repository settings and below it the information which needs to be added when you click on that link. Follow the link and click on **Add webhook**.

   - Here it is crucial to add the correct webhook secret provided by the CSCS-CI administrator (currently Andreas Fink).
   - **Which events would you like to trigger this webhook?**  "Just the push event" will literally only trigger the notification if you perform **git push**.  It may be easier initially to enable 'Send me everything', as suggested in the instructions.
   - If and when things do not work, you go to "Webhooks" in the repository settings and edit the webhook (button on right).  In this edit mode one can click on "Recent Deliveries" to see the notifications.  If you click on the individual deliveries, you can see the Request and the Response, who is sometimes helpful with debugging.

- **Add notification token**: this field will start out with three asterisks (i.e., a hidden token) and a green check on the right hand side, leading you to think that nothing needs to be done, but you naturally have to go through the procedure. 

   - Clicking on the little triangle will lead you to well-written but voluminous GitHub documentation. The page you actually want to read is: https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token#creating-a-fine-grained-personal-access-token.

   - You can specify only specific repositories but "All repositories" (which you have access to) is a safe bet. Not that under permissions (for both Repository and Account permissions) by default you have "No access" to any of the categories.  At the very least you must change **Commit statuses** to read/write access. Why write? because the middleware wants to write the commit status to the pipeline.

   - Once the token is generated, you will see it exactly this once, so copy it immediately into your set-up. You will not be able to go back later on to pick it up, and if you do miss the opportunity, the best procedure is to delete existing tokens and generate new ones.

### Understanding when CI is triggered
#### Push events
- Every pipeline can define its own list of CI-enabled branches
- If a pipeline does not define a list of CI-enabled branches, the global list will be used
- If you push changes to a branch every pipeline that has this branch in its list of CI-enabled branches will be triggered
- If the global list and all pipelines have an empty list of CI-enabled branches, then CI will never be triggered on push events

#### Pull requests (Merge requests)
- For simplicity we will call it PR, although some providers call it Merge request. It is the same thing.
- Every pipeline can define its own list of whitelisted users.
- If a pipeline does not define a list of whitelisted users, the global list will be used.
- If a PR is opened/edited and targets a CI-enabled branch, and the source branch is not from a fork, then all pipelines will be started that have the target branch in its list of CI-enbalded branches.
- If a PR is opened/edited and targets a CI-enabled branch, but the source branch is from a fork, then a pipeline will be automatically started if and only if the fork is from a user in the pipeline's whitelisted user list and the target branch is in the pipeline's CI-enabled branches.

#### `cscs-ci run` comment
- You have an open PR
- You want to trigger a specific pipeline
- Write a comment inside the PR with the text `cscs-ci run PIPELINE_NAME_1,PIPELINE_NAME_2`
- Special case: You have only one pipeline, then you can skip the pipeline names and write only the comment `cscs-ci run`
- The pipeline will only be triggered, if the commenting user is in the pipeline's whitelisted users list.
- The target branch is ignored, i.e. you can test a pipeline even if the target branch is not in the pipeline's CI-enabled branches.

### Understanding the underlying workflow
Typical users do not need to know the underlying workflow behind the scenes, so you can stop reading here. However, it might put the above-mentioned steps into perspective.  It also can give you background for inquirying if and when something in the procedure does not go as expected.

#### Workflow

1. (Prerequisite) icon-exclaim will have a webhook setup
1. You make some change in the icon-exclaim repository
1. Github sends a webhook to cicd-ext-mw.cscs.ch (CI middleware)
1. A technical detail, absolutely unimportant for the user to know what's happening here: CI middleware fetches your repository from github and pushes a mirror to gitlab
1. Gitlab sees a change in the repository and starts a pipeline (i.e. it uses the CI yaml as entrypoint)
1. If the repository is recursive, GIT_SUBMODULE_STRATEGY had to be specified and then the whole mirror repository will be cloned recursively
1. The specified runner, which has as input a Dockerfile (specified in the variable DOCKERFILE), will take this Dockerfile and execute docker build -f $DOCKERFILE ., where the build context is the whole (recursively) cloned repository

## Example projects
Here are a couple of projects which use this CI setup. Please have a look there for more advanced usage:
- [dcomex-framework](https://github.com/DComEX/dcomex-framework): entrypoint is `ci/prototype.yml`
- [utopia](https://bitbucket.org/zulianp/utopia/src/development/): two pipelines, with entrypoints  `ci/cscs/mc/gitlab-daint.yml` and `ci/cscs/gpu/gitlab-daint.yml`
- [mars](https://bitbucket.org/zulianp/mars/src/development/): two pipelines, with entrypoints `ci/gitlab/cscs/gpu/gitlab-daint.yml` and `ci/gitlab/cscs/mc/gitlab-daint.yml`
- [sparse_accumulation](https://github.com/lab-cosmo/sparse_accumulation): entrypoint is `ci/pipeline.yml`
- [gt4py](https://github.com/GridTools/gt4py): entrypoint is `ci/cscs-ci.yml`
- [SIRIUS](https://github.com/electronic-structure/SIRIUS): entrypoint is `ci/cscs-daint.yml`
- [sphericart](https://github.com/lab-cosmo/sphericart): entrypoint is `ci/pipeline.yml`
